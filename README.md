# IP Sniffer

Check all the open ports available to a specific IP address following the implementation from https://youtu.be/-Jp7sabBCp4.

## Usage

```shell
cargo run -- -h 
cargo run -- -j 1000 192.168.0.1
cargo run -- 192.168.0.1
```